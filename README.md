# Create FATHMM-DB

## Create container named fathmmdb
```
$ docker build -t fathmmdb .
$ docker run -p3306:3306 --name=fathmmdb --env="MYSQL_ROOT_PASSWORD=mypw" fathmmdb
```
## Connect to container and mysql
```
$ docker exec -it fathmmdb bash
$ mysql -uroot -p -h 127.0.0.1
```

## create and fill database named fathmmdb
```
mysql> CREATE DATABASE fathmmdb;
mysql> exit
$ exit
```
## go to folder, where fathmm.v2.3.SQL.gz is located then execute the following command 
This this command fills the data base and needs some time. This Can take severall hours, while there is no response in the terminal.
```
$ zcat fathmm.v2.3.SQL.gz | tr -d '"' | docker exec -i fathmmdb sh -c 'exec mysql --force -uroot -pmypw fathmmdb'
```
